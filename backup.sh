#!/bin/sh

base="$(cd "$(dirname "$0")" && pwd)"
backup_dir="/mnt/drive/site-backups/"
[ ! -d "$backup_dir" ] && exit 1

mkdir -p "$backup_dir"

function backup_db {
	local cmd="pg_dump -F c -U postgres $3 \"$1\""
	local out_file="$backup_dir/${1}_$(date +%F_%H-%M-%S).dump"

	echo -n "$(tput setaf 4)>>$(tput sgr0) Backing up ${1}..."
	ssh $2 $cmd > "$out_file"
	size=$(du -b -h "$out_file" | cut -f 1)
	echo " $size ($out_file)"
}

backup_db itchio "-p 1338 itch@itch.io" "--exclude-table-data=referrers_daily --exclude-table-data=exception_requests --exclude-table-data=archived_referrers_daily --exclude-table-data=stripe_webhooks"
#backup_db itchio "-p 1338 itch@itch.io" "--exclude-table-data=exception_requests"
backup_db moonrocks "-p 1338 luarocks@luarocks.org"
backup_db streakclub "-p 1338 linode.leafo.net"