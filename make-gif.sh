#!/bin/sh

function log {
	echo "$(tput setaf 4)>>$(tput sgr0) $@"
}

default_delay=5
delay=${1:-$default_delay}

log "converting... (delay=$delay)"

convert -delay $delay -loop 0 $(ls *.png *.jpg | sort -V) _tmp.gif
log "converted $(du -b -h "_tmp.gif" | cut -f 1)"

gifsicle --colors 256 -O2 _tmp.gif -o out.gif
log "crushed $(du -b -h "out.gif" | cut -f 1)"

rm _tmp.gif
