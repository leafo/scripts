#!/bin/bash

dir=sorted
dir=${1:-$dir}

mkdir -p $dir

count=0
count=${2:-$count}
echo "starting at" $count "dir:" $dir

while read line; do
	ext="${line##*.}"
	dest="$dir/$(printf "%09d" $count).$ext"
	echo $line "->" $dest
	mv "$line" "$dest"
	count=$(($count+1))
done
